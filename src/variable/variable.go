package variable

import (
	"fmt"
	"math"
	//	"os"
	w "../word"
	"reflect"
	"strconv"
	"unsafe"
)

func Variable() {

	s := w.Startline
	s("Variable")

	defer w.Endline()

	var a int
	w.Printwithline("var a int, Default a is", a)

	var c = false
	w.Printwithline("var c = false, Default c's type is", reflect.TypeOf(c))

	//var d, e int
	w.Printwithline("var d,e int", "Multiple same type")

	//var f, g = 100, "abc"
	w.Printwithline("var f, g = 100, \"abc\"", "Multiple different type")

	//
	// var (
	// 	d, e int
	// 	f, g = 100, "abc"
	// )
	w.Descriptionwithline("var (\n\td, e int\n\tf, g = 100, \"abc\"\n)", "Multiple line")

	//
	// var (
	// 	d, e int
	// 	f, g = 100, "abc"
	// )
	w.Descriptionwithline("var (\n\td := 100\n\tf, g := 100, \"abc\"\n)", "Multiple line short mode")

	fmt.Println("h := 1\n{\n\th := 2\n}")

	h := 1

	fmt.Println("h := 1 =>", &h, h)

	{
		h := 2

		fmt.Println("h := 2 =>", &h, h)

	}

	w.Descriptionwithline("redefine in deferent domain")

	// f, err := os.Open("/dev/random")

	// println(f, err)
	// buf := make([]byte, 1024)
	// n, err := f.Read(buf)

	// println(n, err)

	// x, y := 1, 2
	// x, y = y+3, x+2
	// fmt.Println(x, y)

	const (
		x, y = 99, -999
		b    = byte(x)
		//n    = uint8(y) constant -999 overflows uint8
	)

	const (
		ptrSize = unsafe.Sizeof(uintptr(1999))
		strSize = len("hi")
	)

	baseType()
	enum()
	referenceType()
	typeTransfor()
	customType()
	unnamedType()
}

func unnamedType() {

	//wrong example
	// var a struct {
	// 	x int    "x"
	// 	s string "s"
	// }
	// var b struct {
	// 	x int
	// 	s string
	// }
	// b = a //cannot use a (type struct { x int "x"; s string "s" }) as type struct { x int; s string } in assignment

	// var a func(int, string)
	// var b func(string, int)

	// b = a //cannot use a (type func(int, string)) as type func(string, int) in assignment

}

func customType() {

	type flages byte

	const (
		read flages = 1 << iota
		write
		exec
	)

	f := read | exec
	fmt.Printf("%b\n", f)

	type (
		user struct {
			name string
			age  uint8
		}
		event func(string) bool
	)

	u := user{"Tom", 20}
	fmt.Println(u)

	var j event = func(s string) bool {
		fmt.Println(s)
		return s != ""
	}
	j("abc")

	//wrong example

	// type data int
	// var d data = 10
	// var x int = d //cannot use d (type data) as type int in assignment

	// fmt.Println(d == x)//invalid operation: d == x (mismatched types data and int)
}

func typeTransfor() {
	defer w.Line()
	a := 10
	b := byte(a)
	c := a + int(b)

	fmt.Println("a:", a, reflect.TypeOf(a))
	fmt.Println("b:", b, reflect.TypeOf(b))
	fmt.Println("c:", c, reflect.TypeOf(c))

	x := 100
	p := (*int)(&x)

	fmt.Println(p)
}

func referenceType() {
	defer w.Line()
	s := make([]int, 0, 2)
	fmt.Println(s, len(s))
	for i := 0; i < 10; i++ {
		s = append(s, i)
		fmt.Println(s, len(s))
	}

	w.Line()

	m := make(map[string]int)
	m["a"] = 1
	m["b"] = 2
	m["b"] = 4
	m["c"] = 3
	m["d"] = 3
	fmt.Println(m, len(m), m["c"])
}

func baseType() {

	defer w.Line()
	a, b, c := 100, 0144, 0x64
	d := 1100100
	fmt.Println(a, b, c, d)
	fmt.Printf("0b%v\n", d)
	fmt.Printf("0b%b, %#o, %#X\n", a, a, a)

	fmt.Println(math.MinInt8, math.MaxInt8)

	e, _ := strconv.ParseInt("1100100", 2, 32)

	f, _ := strconv.ParseInt("0144", 8, 32)

	g, _ := strconv.ParseInt("64", 16, 32)

	fmt.Println(e, f, g)

	fmt.Println("0b" + strconv.FormatInt(e, 2))
	fmt.Println("0o" + strconv.FormatInt(e, 8))
	fmt.Println("0x" + strconv.FormatInt(e, 16))

	h := fmt.Sprintf("0b%v", d)

	fmt.Println(h)

	//注意被float32截斷的位數
	var i, j, k float32 = 1.1234567899, 1.12345678, 1.123456781

	fmt.Println(i, j, k)

	fmt.Println(i == j, j == k)

	fmt.Printf("%v %v %v\n", i, j, k)
}

func enum() {

	type AudioOutput int

	const (
		OutMute   AudioOutput = iota // 0
		OutMono                      // 1
		OutStereo                    // 2
		_
		_
		OutSurround // 5
	)

	//fmt.Println(OutSurround)

	type Allergen int

	const (
		IgEggs         Allergen = 1 << iota // 1 << 0 which is 00000001
		IgChocolate                         // 1 << 1 which is 00000010
		IgNuts                              // 1 << 2 which is 00000100
		IgStrawberries                      // 1 << 3 which is 00001000
		IgShellfish                         // 1 << 4 which is 00010000
	)
	//fmt.Println(IgEggs)
	//fmt.Println(IgEggs | IgChocolate | IgShellfish)

	type ByteSize float64

	const (
		_           = iota             // ignore first value by assigning to blank identifier
		KB ByteSize = 1 << (10 * iota) // 1 << (10*1)
		MB                             // 1 << (10*2)
		GB                             // 1 << (10*3)
		TB                             // 1 << (10*4)
		PB                             // 1 << (10*5)
		EB                             // 1 << (10*6)
		ZB                             // 1 << (10*7)
		YB                             // 1 << (10*8)
	)
	//fmt.Println(YB)

	const (
		Apple, Banana = iota + 1, iota + 2
		Cherimoya, Durian
		Elderberry, Fig
	)

	// Apple: 1
	// Banana: 2
	// Cherimoya: 2
	// Durian: 3
	// Elderberry: 3
	// Fig: 4

	const (
		a = iota
		b
		c = 100
		d
		e = iota
		f
	)

	//fmt.Println(a, b, c, d, e, f)

}

package word

import (
	"fmt"
)

func Startline(title string) {
	fmt.Println(buildline(15, "=") + " " + title + " " + buildline(15, "=") + "\n")
}

func Endline() {
	fmt.Println(buildline(40, "="))
}

func Line() {
	fmt.Println("\n" + buildline(40, "-") + "\n")
}

func buildline(count int, symbol string) string {
	tmpstr := ""
	for i := 0; i < count; i++ {
		tmpstr += symbol
	}
	return tmpstr
}

func Printdescription(description string) {
	fmt.Println("\n" + description)
}

func Descriptionwithline(message ...string) {
	for index, element := range message {
		if index == len(message)-1 {
			Printdescription(element)
			Line()
		} else {
			fmt.Println(element)
		}
	}
}

func Printwithline(message ...interface{}) {
	for index, element := range message {
		if index > 0 {
			fmt.Print(" ")
		}
		fmt.Print(element)
	}
	fmt.Print("\n")
	Line()
}

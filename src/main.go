package main

import (
	"errors"
	"fmt"
	//	"os"
	v "../src/variable"
)

type user struct {
	name string
	age  byte
}

type manager struct {
	user
	title string
}

func expression() {

}

func main() {

	v.Variable()

	expression()
	// a, b := 10, 5
	// c, d, err := div(a, b)
	// fmt.Println(c, d, err)

	// x := make([]int, 0, 5)
	// for i := 0; i < 8; i++ {
	// 	x = append(x, i)
	// }
	// fmt.Println(x)

	// for i, n := range x {
	// 	x[i] += n
	// }
	// fmt.Println(x)

	// w := 3
	// switch {
	// case w > 3:
	// 	fmt.Println("w>3")
	// case w > 2:
	// 	fmt.Println("w>2")
	// case w > 1:
	// 	fmt.Println("w>1")
	// }

	// switch {
	// case w > 0:
	// case w > 1:
	// 	fmt.Println("w>1")
	// case w > 2:
	// 	fmt.Println("w>2")
	// case w > 3:
	// 	fmt.Println("w>3")
	// }

	// m := make(map[string]int)
	// m["a"] = 3
	// //m["b"] = 3
	// y, ok := m["a"]
	// fmt.Println(y, ok)
	// delete(m, "a")
	// z, ok := m["a"]
	// fmt.Println(z, ok)

	// g := closure(1)
	// g()
	// g()
	// g()

	// var n manager

	// n.name = "tom"
	// n.age = 40
	// n.title = "CEO"

	// fmt.Println(n)

}

func div(a, b int) (int, int, error) {
	defer fmt.Println(a)
	if b == 0 {
		return 0, 0, errors.New("div by zero")
	}

	return a / b, 3, nil
}

func closure(g int) func() {
	return func() {
		g++
		fmt.Println(g)
	}
}
